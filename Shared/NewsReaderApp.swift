//
//  NewsReaderApp.swift
//  Shared
//
//  Created by Markus Syrjälä on 27.3.2021.
//

import SwiftUI

@main
struct HackerNewsApp: App {
  var body: some Scene {
    WindowGroup {
      #if os(macOS)
      NavigationView {
        Sidebar()
        ItemsListView(viewModel: ItemsViewModel(category: .top))
      }
      #else
      NavigationView {
        TabBar()
          .navigationTitle("Hacker News")
      }.navigationViewStyle(StackNavigationViewStyle())
      #endif
    }
  }
}
